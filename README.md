## Как использовать

Для запуска вы можете исползьзовать 3 варианта
 
### 1. Установка вручную
Для запуска сервера вам потребуется python 3.9 с библиотекой flask. Установите их любым удобным способом
```
python server.py --port [PORT]
```
где `[PORT]` - порт на котором вы хотите слушать соединения.

### 2. Установка с использование anaconda
Установите miniconda следуя следующим инструкциям https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html
Затем для установки необходимого окружения запустите
```
conda env create -f environment.yml
```
Для активации окружения запустите
```
conda activate confocal
```
Далее запустите сервер
```
python server.py --port [PORT]
```
где `[PORT]` - порт на котором вы хотите слушать соединения.

### 3. Если у вас linux и установлен docker
Есть также docker image на базе `python:3.9-alpine`
```
docker run akkyma232/confocal:v0.1 -p [PORT]:5000 -d
```
где `[PORT]` - порт на котором вы хотите слушать соединения.
