import argparse
from flask import Flask, request, send_from_directory
from flask import render_template

parser = argparse.ArgumentParser()
parser.add_argument("--port", default=5000, required=False)
args = parser.parse_args()

app = Flask(__name__, static_url_path='', static_folder="static", template_folder='templates')

@app.after_request
def after_request(response):
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate, public, max-age=0"
    response.headers["Expires"] = '0'
    response.headers["Pragma"] = "no-cache"
    return response

@app.route('/textures/<path:path>')
def send_textures(path):
    return send_from_directory('textures', path)

@app.route('/resources/<path:path>')
def send_resources(path):
    return send_from_directory('resources', path)

@app.route('/')
def index():
    return render_template('visualizer.html', species="rugicollis")


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=args.port, debug=False)
