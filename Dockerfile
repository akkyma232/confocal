FROM python:3.9-alpine
RUN addgroup -g 2000 user \
    && adduser -u 2000 -G user -s /bin/sh -D user
USER user
WORKDIR app
RUN pip install flask
COPY . .
ENTRYPOINT ["python3", "server.py"]
