import * as THREE from 'https://unpkg.com/three@0.126.0/build/three.module.js';

import Stats from 'https://unpkg.com/three@0.126.0/examples/jsm/libs/stats.module.js';

import { GUI } from 'https://unpkg.com/three@0.126.0/examples/jsm/libs/dat.gui.module.js';
import { TrackballControls } from 'https://unpkg.com/three@0.126.0/examples/jsm/controls/TrackballControls.js';
import { NRRDLoader } from 'https://unpkg.com/three@0.126.0/examples/jsm/loaders/NRRDLoader.js';
import { VTKLoader } from 'https://unpkg.com/three@0.126.0/examples/jsm/loaders/VTKLoader.js';
import { GLTFLoader } from 'https://unpkg.com/three@0.126.0/examples/jsm/loaders/GLTFLoader.js';
import { VolumeRenderShader1 } from 'https://unpkg.com/three@0.126.0/examples/jsm/shaders/VolumeShader.js';

function join_path(...names) {
    return names.join("/");
}

function getNorm(...numbers) {
    return Math.sqrt( numbers.reduce((n1, n2) => n1 + n2*n2, 0) );
}

const species = "{{species}}";
const species_dir = join_path("resources", "rugicollis");

const scale_coefs = {
    "low": 0.25,
    "medium": 0.5,
    "high": 1,
}

var container,
    stats,
    current_camera,
    camera1_perspective,
    camera1_orthographic,
    controls,
    controls_per,
    scene,
    renderer;

var container2,
    renderer2,
    camera2,
    axes2,
    scene2;

var volconfig
var material, mesh, selections, light1, light2;
var volumeWidth, volumeHeight, volumeDepth;

const cmtextures = {
    viridis: new THREE.TextureLoader().load( 'textures/cm_viridis.png' ),
    gray: new THREE.TextureLoader().load( 'textures/cm_gray.png' )
};

init();
animate();

function init() {

    volconfig = { 
        image: 'images',
        selections: true,
        quality: "low",
        clim1: 0,
        clim2: 1,
        renderstyle: 'mip',
        isothreshold: 0,
        colormap: 'gray',
        camera: 'orthographic'
    };

    const gui = new GUI();
    gui.add( volconfig, 'image', {
        "original": 'images',
        "edges": 'canny'
    } ).onChange( updateImage );
    gui.add( volconfig, 'quality', [
        "low",
        "medium",
        "high"
    ] ).onChange( updateImage );
    gui.add( volconfig, 'camera', [
        'perspective',
        'orthographic'
    ] ).onChange( updateCamera );
    gui.add( volconfig, 'clim1', 0, 1, 0.01 ).onChange( updateUniforms );
    gui.add( volconfig, 'clim2', 0, 1, 0.01 ).onChange( updateUniforms );
    gui.add( volconfig, 'colormap', { gray: 'gray', viridis: 'viridis' } ).onChange( updateUniforms );
    gui.add( volconfig, 'renderstyle', { mip: 'mip', iso: 'iso' } ).onChange( updateUniforms );
    gui.add( volconfig, 'isothreshold', 0, 1, 0.01 ).onChange( updateUniforms );
    gui.add( volconfig, 'selections' ).onChange( updateSelectionsVisibility );

    scene = new THREE.Scene();

    renderer = new THREE.WebGLRenderer();
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);

    initCameras();
    camera1_perspective.position.x = 1000;
    current_camera.position.x = 4096;

    container = document.createElement('div');
    document.body.appendChild(container);
    container.appendChild(renderer.domElement);

    stats = new Stats();
    container.appendChild(stats.dom);

    setupInset();

    updateImage();

    window.addEventListener('resize', onWindowResize);

}

function initCameras() {
    camera1_orthographic = new THREE.OrthographicCamera( -1, 1, 1, -1, 1, 1e5 );
    camera1_orthographic.position.x = 1;
    camera1_perspective = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 1e5 );
    camera1_perspective.position.x = 1;

    camera1_orthographic.up = camera1_perspective.up = new THREE.Vector3(0, 0, -1);

    scene.add(camera1_orthographic);
    scene.add(camera1_perspective);

    controls = new TrackballControls(camera1_orthographic, renderer.domElement);
    controls.minDistance = 1;
    controls.rotateSpeed = 4.0;
    controls.noPan = true;

    controls_per = new TrackballControls(camera1_perspective, renderer.domElement);
    controls_per.minDistance = 1;
    controls_per.rotateSpeed = 4.0;
    controls_per.noPan = true;

    updateCamera();
}

function resizeOrthographicCamera() {

    var width = volumeWidth;
    var height = volumeHeight;
    if (height / width > innerHeight / innerWidth) {
        width = height * innerWidth / innerHeight;
    } else {
        height = width * innerHeight / innerWidth;
    }

    camera1_orthographic.left = -width / 2;
    camera1_orthographic.right = width / 2;
    camera1_orthographic.bottom = -height / 2;
    camera1_orthographic.top = height / 2;
    camera1_orthographic.updateProjectionMatrix();
}

function resizePerspectiveCamera() {
    camera1_perspective.aspect = window.innerWidth / window.innerHeight;
    camera1_perspective.updateProjectionMatrix();
}

function updateCamera() {
    if (volconfig.camera == 'orthographic') {
        controls.noZoom = false;
        controls_per.noZoom = true;
        current_camera = camera1_orthographic;
    } else if (volconfig.camera == 'perspective') {
        controls.noZoom = true;
        controls_per.noZoom = false;
        current_camera = camera1_perspective;
    }
}

function updateSelectionsVisibility() {
    selections.visible = volconfig.selections;
}

function updateImage() {

    scene.remove(mesh);
    scene.remove(selections);
    scene.remove(light1);
    scene.remove(light2);

    const loader = new NRRDLoader();
    loader.load(join_path(species_dir, volconfig.image, volconfig.quality + ".nrrd"), function (volume) {

        volumeWidth = volume.yLength;
        volumeHeight = volume.zLength;
        volumeDepth = volume.xLength;

        const modulo = getNorm(volumeWidth, volumeHeight, volumeDepth);
        camera1_orthographic.position.copy( camera1_orthographic.position.normalize().multiplyScalar(modulo) );
        camera1_perspective.position.copy( camera1_perspective.position.normalize().multiplyScalar(modulo) );

        const selections_name = join_path(species_dir, "selections.glb");
        new GLTFLoader().load(selections_name, function(gltf) {
            selections = gltf.scene;
            const scale = scale_coefs[volconfig.quality]
            gltf.scene.scale.set(scale, scale, scale);
            scene.add(gltf.scene);
            selections.translateOnAxis( new THREE.Vector3(volume.xLength, volume.yLength, volume.zLength), -0.5 );
        });

        light1 = new THREE.PointLight(0xffffff);
        light1.position.x = -volumeDepth * 3;
        scene.add(light1);

        light2 = new THREE.PointLight(0xffffff);
        light2.position.x = volumeDepth * 3;
        scene.add(light2);

        const texture = new THREE.DataTexture3D( volume.data, volume.xLength, volume.yLength, volume.zLength );
        texture.format = THREE.RedFormat;
        texture.type = THREE.UnsignedByteType;
        texture.minFilter = texture.magFilter = THREE.LinearFilter;
        texture.unpackAlignment = 1;

        const shader = VolumeRenderShader1;
        const uniforms = THREE.UniformsUtils.clone( shader.uniforms );

        uniforms[ "u_data" ].value = texture;
        uniforms[ "u_size" ].value.set( volume.xLength, volume.yLength, volume.zLength );
        uniforms[ "u_clim" ].value.set( volconfig.clim1, volconfig.clim2 );
        uniforms[ "u_renderstyle" ].value = volconfig.renderstyle == 'mip' ? 0 : 1; // 0: MIP, 1: ISO
        uniforms[ "u_renderthreshold" ].value = volconfig.isothreshold; // For ISO renderstyle
        uniforms[ "u_cmdata" ].value = cmtextures[ volconfig.colormap ];

        material = new THREE.ShaderMaterial({
            uniforms: uniforms,
            vertexShader: shader.vertexShader,
            fragmentShader: shader.fragmentShader,
            side: THREE.BackSide // The volume shader uses the backface as its "reference point"
        });

        const geometry = new THREE.BoxBufferGeometry( volume.xLength, volume.yLength, volume.zLength );
        geometry.translate( volume.xLength / 2, volume.yLength / 2, volume.zLength / 2 );

        mesh = new THREE.Mesh( geometry, material );
        mesh.translateOnAxis( new THREE.Vector3(volume.xLength, volume.yLength, volume.zLength), -0.5 );
        scene.add( mesh );

        onWindowResize();
    });
}

function updateUniforms() {

    material.uniforms[ "u_clim" ].value.set( volconfig.clim1, volconfig.clim2 );
    material.uniforms[ "u_renderstyle" ].value = volconfig.renderstyle == 'mip' ? 0 : 1;
    material.uniforms[ "u_renderthreshold" ].value = volconfig.isothreshold;
    material.uniforms[ "u_cmdata" ].value = cmtextures[ volconfig.colormap ];
}

function onWindowResize() {

    resizeOrthographicCamera();
    resizePerspectiveCamera();

    renderer.setSize(window.innerWidth, window.innerHeight);

    controls.handleResize();
    controls_per.handleResize();
}

function animate() {

    requestAnimationFrame(animate);

    controls.update();
    controls_per.update();

    //copy position of the camera into inset
    camera2.position.copy(current_camera.position);
    camera2.position.sub(controls.target);
    camera2.position.setLength(300);
    camera2.lookAt(scene2.position);

    renderer.render(scene, current_camera);
    renderer2.render(scene2, camera2);

    stats.update();

}

function setupInset() {

    const insetWidth = 150, insetHeight = 150;
    container2 = document.getElementById('inset');
    container2.width = insetWidth;
    container2.height = insetHeight;

    // renderer
    renderer2 = new THREE.WebGLRenderer({ alpha: true });
    renderer2.setClearColor(0x000000, 0);
    renderer2.setSize(insetWidth, insetHeight);
    container2.appendChild(renderer2.domElement);

    // scene
    scene2 = new THREE.Scene();

    // camera
    camera2 = new THREE.PerspectiveCamera(50, insetWidth / insetHeight, 1, 1000);
    camera2.up = current_camera.up; // important!

    // axes
    axes2 = new THREE.AxesHelper(100);
    scene2.add(axes2);

}
